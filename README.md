# Terrawsible

A docker image based on the latest alpine featuring terraform, ansible and the awscli

# see https://hub.docker.com/r/martinstiehr/terrawsible

docker run -it martinstiehr/terrawsible
