FROM alpine:latest
LABEL maintainer="m.stiehr@gmail.com"

RUN apk update && \
    apk add openssh-client terraform ansible python3 py3-pip && \
    pip3 install --upgrade pip && \
    pip3 install awscli boto3 && \
    mkdir ~/.ssh

